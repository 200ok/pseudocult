# Welcome to Pseudocult

Pseudocult (which is an anagram to "cloud setup") enables you to set
up your cloud (AWS) declarativly and consistently.

It is mainly a clojure program that you run providing one or more
config files, which describe your cloud setup. Pseudocult will detect
discrepancies between the current state of the cloud and the
description obtained from the config files as well as take action to
overcome the discrepencies.

Config files can be written in YAML or EDN.

### Example

```
# config.yml
s3:
  - name: pseudocult-example
    region: eu-central-1
```

Running pseudocult with this config, will check if a bucket with the
name `pseudocult-example` exists and if not will create it in region
`eu-central-1`.

### Mode of Operation

- Idempotent, build with repeated application in mind
- Nondestructive, only cares about the things it knows about
- ...

## AWS Profiles

If you work with AWS Profiles you can set you profile when starting
the repl

    AWS_PROFILE=<your-profile> lein repl

## License

Copyright © 2017 phil@200ok.ch

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
