(ns pseudocult.config
  "Means to read data form files of different formats and merge them
  together to a single configuration."
  (:require [clojure.string :as str]
            [clojure.walk :refer [keywordize-keys]]
            [yaml.core :as yaml]))

(defn- file-extension
  "Extract the extension from filename and return it as keyword."
  [filename]
  (keyword (last (str/split filename #"\."))))

(defmulti read-file
  "Implementations for reading & parsing files into data structures,
  depending on file extension."
  file-extension)

(defmethod read-file :default [filename]
  (println (str "No method read-file for "
                (name (file-extension filename))
                ", skipping " filename))
  {})

(defmethod read-file :edn [filename]
  (-> filename
      slurp
      read-string))

(defmethod read-file :yml [filename]
  (-> filename
      yaml/from-file
      keywordize-keys))

(defn config
  "Reads & parses config files and merges them into one
  configuration."
  [args]
  (apply merge (map read-file args)))
