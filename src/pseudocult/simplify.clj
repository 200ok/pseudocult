(ns pseudocult.simplify
  "Means to simplify the output. Transforming hard to read objects
  into readable representations."
  (:require [clojure.walk :refer [postwalk]]
            [pseudocult.config :refer [config]]))

(defmulti transform
  "Implementations of transformations depending on type."
  type)

(defmethod transform :default [form] form)

(defmethod transform org.joda.time.DateTime [form]
  (str form))

(defn simplify [config]
  (postwalk transform config))
