(ns pseudocult.services.s3
  "Pseudocult service for AWS S3."
  (:require [amazonica.aws.s3 :refer [list-buckets]]
            [pseudocult.service :as pcs]))

(defn- missing-from
  "Returns true if the given bucket does not exist in buckets."
  [buckets bucket]
  (not-any? #{(:name bucket)} (map :name buckets)))

(defmethod pcs/service :s3 [_ config]
  (let [existing (list-buckets)
        missing (filter (partial missing-from existing) config)]
    ;; TODO create missing buckets
    (prn :s3/missing missing)
    ;; TODO list buckets again and return those
    existing
    ))
