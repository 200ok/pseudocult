(ns pseudocult.services.sns
  "Pseudocult service for AWS SNS."
  (:require [clojure.data.json :as json]
            [clojure.string :refer [split]]
            [clojure.walk :refer [postwalk]]
            [amazonica.aws.sns :refer [list-topics
                                       get-topic-attributes
                                       create-topic
                                       subscribe
                                       list-subscriptions]]
            [pseudocult.service :as pcs]
            [clojure.string :as str]))


(defn- missing-from
  "Returns true if the given topic does not exist in topics."
  [topics topic]
  (not-any? #{(:name topic)} (map :name topics)))

(defn- keyword-converter
    "Given something that tokenizes a string into parts, turn it into
  a :kebab-case-keyword."
  [separator-regex]
  (fn [s]
    (->> (str/split s separator-regex)
         (map str/lower-case)
         (interpose \-)
         str/join
         keyword)))

(def ^:private camel->keyword
  "from Emerick, Grande, Carper 2012 p.70"
  (keyword-converter #"(?<=[a-z])(?=[A-Z])"))

(defn- transform-keys [f m]
  (let [g (fn [[k v]] [(f k) v])]
    (postwalk (fn [x] (if (map? x) (into {} (map g x)) x)) m)))

(def ^:private camel->keyword-keys
  (partial transform-keys (comp camel->keyword name)))

(def ^:private break-json
  (comp camel->keyword-keys json/read-str))

(defn- fix [attributes]
  (-> (camel->keyword-keys attributes)
      (update :policy break-json)
      (update :effective-delivery-policy break-json)))

(defn- list-by-topic [result subscription]
  (let [topic-arn (:topic-arn subscription)]
    (update result topic-arn conj subscription)))

(defn- subscriptions-by-topic []
  (reduce list-by-topic {} (:subscriptions (list-subscriptions))))

(defn- topics []
  (let [subscriptions (subscriptions-by-topic)]
    (->> (:topics (list-topics))
         (map (comp fix :attributes get-topic-attributes :topic-arn))
         (map #(assoc % :subscriptions (subscriptions (:topic-arn %) [])))
         )))

(defn- filter-missing [subscriptions topic existing]
  (let [existing-set (->> existing
                          (map unwrap-name)
                          (map (partial-right select-keys crucial-keys))
                          set)]
    (filter (partial is-missing-subscription? existing-set (:name topic))
            subscriptions)))


(defn- aggregate-subscriptions [subscriptions name]
  (map #(assoc % :name name) subscriptions))

(defn- aggregate-topic-subscriptions [topic]
  (update topic :subscriptions aggregate-subscriptions (:name topic)))

(defn- limit-to-crucial-keys [subscription]
  (select-keys subscription [:name :endpoint :protocol]))

(defn- unwrap-name [resource]
  (assoc resource :name
         (-> resource :topic-arn (split #":") last)))

(defn- build-existing-set [existing]
  (set (map (comp limit-to-crucial-keys unwrap-name) existing)))

(defn- build-expected [topics]
  (->> topics
       (mapcat (comp :subscriptions aggregate-topic-subscriptions))
       (map limit-to-crucial-keys)))

(defn- find-missing-subscriptions [existing topics]
  (let [existing-set (build-existing-set existing)
        expected (build-expected topics)]
    (filter (partial not-any? existing-set) expected)))

(defn- build-name-to-arn [topics]
  (->> topics
       (map (comp (fn [%] {(:name %) (:topic-arn %)}) unwrap-name))
       (apply merge)))

(defn- create-missing-subscription
  [name-to-arn {:keys [name endpoint protocol]}]
  (let [topic-arn (name-to-arn name)]
    (prn topic-arn)
    (prn protocol)
    (prn endpoint)
    ;; this fails with InvalidParameter
    ;; could be that it fails because the endpoint does not respond?
    (subscribe topic-arn protocol endpoint)))

(defmethod pcs/service :sns [_ config]
  (let [existing-topics (:topics (list-topics))
        missing-topics (filter (partial missing-from existing-topics) config)
        existing-subscriptions (:subscriptions (list-subscriptions))
        missing-subscriptions
        (find-missing-subscriptions existing-subscriptions config)
        name-to-arn (build-name-to-arn existing-topics)
        ]

    (dorun (map create-topic missing-topics))
    (dorun (map (partial create-missing-subscription name-to-arn)
                missing-subscriptions))
    (topics)

    ))
