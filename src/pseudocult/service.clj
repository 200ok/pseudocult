(ns pseudocult.service
  "Holds the default (dummy) implementaion for all pseudocult
  services.")

(defmulti service
  "Implements strategies for different services."
  (fn [key _] key))

(defmethod service :default [key config]
  (prn "Service" key "not yet implemented.")
  config)
