(ns pseudocult.core
  "Pseudocult (which is an anagram to 'cloud setup') enables you to
  set up your cloud (AWS) declarativly and consistently.

  It is mainly a clojure program that you run providing one or more
  config files, which describe your cloud setup. Pseudocult will
  detect discrepancies between the current state of the cloud and the
  description obtained from the config files as well as take action to
  overcome the discrepencies.

  Config files can be written in YAML or EDN."
  (:gen-class)
  (:require [clojure.pprint :refer [pprint]]
            [pseudocult
             [config :refer [config]]
             [service :refer [service]]
             [services]
             [simplify :refer [simplify]]]
            [yaml.core :as yaml]))

(defn- run-service
  "Runs a single service merging the result."
  [result key config]
  (assoc result key (service key config)))

(defn- yamlify [x]
  (yaml/generate-string x :dumper-options {:flow-style :block}))

(defn -main
  "Reads config, processes it and pretty prints the result."
  [& args]
  (let [result (simplify (reduce-kv run-service {} (config args)))]
    ;;(pprint result)
    (println (yamlify result))
    ))
