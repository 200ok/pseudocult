(ns pseudocult.services
  "This is just a dummy namespace which requires all the services so
  they get actually loaded."
  (:require [pseudocult.services.s3]
            [pseudocult.services.sns]))
